<?php
    $sql_footer = "SELECT * from home_text"; 
    $result_footer = mysqli_query($conn, $sql_footer);

?>
<div class="footer">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3" >
				<div class="footer_logo">
					<img src="../imgs/logo_header.png" class="imgs">
				</div>
			</div>
			<div class="col-md-3">
				<div class="footer-title">
					<p>
					eod tactical 
					solutions inc.
					</p>
				</div>
				<div class="footer-text">
					<?php
							while($row_footer= mysqli_fetch_array($result_footer)) {
						?>
						<?php  
							if($row_footer["title"]=="footer_text"){
						?>
							<p  id="footer_text" >
									
									<?php echo $row_footer["content"]; ?>
							</p><?php  }}?>
				</div>
			</div>
			<div class="col-md-3">
				<div class="footer-title">
					<br>
					<p>
					useful links</p>
				</div>
				<div class="footer-text">
					<div class="icon"><i class="ion-play" ></i>
					 		<span> <a href="./?r=<?php echo base64_encode("1");?>&j=<?php echo base64_encode(rand()); ?>&p=home"> HOME </a></span>
					</div>
					<div class="icon"><i class="ion-play" ></i>
					 		<span> <a  href="./?r=<?php echo base64_encode("2");?>&j=<?php echo base64_encode(rand()); ?>p=courses"  >COURSES </a></span>
					</div>
					<div class="icon"><i class="ion-play" ></i>
					 		<span> <a href="./?r=<?php echo base64_encode("3");?>&j=<?php echo base64_encode(rand()); ?>p=events"  >EVENTS</a></span>
					</div>
					<div class="icon"><i class="ion-play" ></i>
					 		<span> <a href="./?r=<?php echo base64_encode("4");?>&j=<?php echo base64_encode(rand()); ?>p=about"  >ABOUT US </a></span>
					</div>
					<!-- <div class="icon"><i class="ion-play" ></i>
					 		<span>SEND US MESSAGES</span>
					</div> -->
				</div>

			</div>
			<div class="col-md-3">
				<div class="footer-title"><br>
					<p>
					Contact Us</p>
					</div>
				<div class="footer-text">

				<div>	
					<a href="">
						<i class="fa fa-home" aria-hidden="true"></i>
						<span>Tarlac City</span>
					</a> 
				</div>
				<div>	
					<a href="">
						<i class="fa fa-phone" aria-hidden="true"></i>
						<span>0928 443 8888</span>
					</a>
				</div>
				<div>
					<a href="">
						<i class="fa fa-envelope" aria-hidden="true"></i>
						<span>eodtacsolutions@gmail.com</span>
					</a>
				</div>
				<div>
					<a href="">
						<i class="fa fa-facebook" aria-hidden="true"></i>
						<span>facecook.com/Eodtacticalsolutions</span>
					</a>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="footer-end">
	Copyright © 2018 EoD Tactical Solutions Inc. All rights reserved
</div>