<div class="top-stat" >
<div class="statistics" >
	<div class="container" >
		<div class="row">
			<div class="col-md-3"  >
						<div class="content" >
		  						<i  class="fas fa-user fa-stack-1x  serviceicon"></i>
		  					<div class="title">
		  						<h1>5000</h1>
		  						<h6>STUDENTS</h6>
		  					</div>
		  				</div>
						
			</div>
			<div class="col-md-3">
						
						<div class="content">
		  						<i  class="far fa-file-alt fa-stack-1x  serviceicon"></i>
		  					<div class="title">
		  						<h1>20</h1>
		  						<h6>COURSES</h6>
		  					</div>
		  				</div>
			</div>
			<div class="col-md-3">
						<div class="content">
		  						<i  class="far fa-copy fa-stack-1x  serviceicon"></i>
		  					<div class="title">
		  						<h1>300</h1>
		  						<h6>MODULES</h6>
		  					</div>
		  				</div>
			</div>
			<div class="col-md-3">
						<div class="content">
		  					<i class="fas fa-chalkboard-teacher fa-stack-1x  serviceicon"></i>
		  					<div class="title">
		  						<h1>20</h1>
		  						<h6>INSTRUCTORS</h6>
		  					</div>
		  				</div>
			</div>
		</div>
	</div>
</div>
</div>