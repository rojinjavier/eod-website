
	<?php
    $sqla = "SELECT id,content,title from announcement ORDER BY id DESC"; 
    $resulta = mysqli_query($conn, $sqla);
    $sql_title = "SELECT * from home_text"; 
    $result_title = mysqli_query($conn, $sql_title);
	?>
<div style=" background-color: #cbcbcb">
<div class="announcements">
	<div class="container">
		<div class="announcements-title">
			ANNOUNCEMENTS
		</div>

		<div class="announcements-sub">
						<?php
							while($row_title = mysqli_fetch_array($result_title)) {
						?>
						<?php  
							if($row_title["title"]=="announcement_sub"){
						?>
							<p  id="announcement_sub">
									
									<?php echo $row_title["content"]; ?>
							</p><?php  }}?>

		</div>
		<div class="announcements-content">
		<div class="row">
					<?php
						while($rowa = mysqli_fetch_array($resulta)) {
					?>
			<div class="col-md-6">
				<div class="card">
						 <img src="../php/announcement-view.php?image_id=<?php echo $rowa["id"]; ?>"  />
				 			
						  <div class="card-footer">
						  		<div class="card-footer-title">
						  					<?php echo $rowa['title'];?>
						  		
						  		</div>
						  		<div class="place">

						  			<!-- 
								    	<button type="button" id="one-announcement"  >CUBAO</button> -->
								 
						  		</div>
						  		<div class="card-footer-text">
						  					<?php echo $rowa['content'];?>
						  		</div>
						  		<div class="form-row text-center">
								    <div class="col-12">
								    	<button type="button" id="announcements-readmore" class="btn "  onclick="readmore('<?php echo $rowa["id"]; ?>' , '<?php echo $rowa["title"]; ?>' , '<?php echo $rowa["content"]; ?>'  )" >READ MORE</button>
								    </div>
								</div>
						  		
						  </div>
						</div>
			</div>
			
				<?php		
			}
	
		?>
			 
		</div>

		</div>

	</div>
</div>
</div>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
      	<img id="imgs">
        <div class="modal-header">
  			
          <div class="modal-title" id="title" style="font-size: 24px;font-weight: bold"></div>
        </div>
        <div class="modal-body">
          <p id="desc"></p>
    
        </div>
        <div class="modal-footer">
    
           <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	function readmore(a,b,c){
	document.getElementById('imgs').src = '../php/announcement-view.php?image_id='+a;
	document.getElementById('title').innerHTML = b;
	document.getElementById('desc').innerHTML = c;

$('#myModal').modal('show') ;


	}


	
</script>
