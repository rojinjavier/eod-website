<?php
  
    $sql_about = "SELECT * from home_text"; 
    $result_about = mysqli_query($conn, $sql_about);

?>

<div class="container-fluid" style="background-color: #fee84d;">
<div class="container"   >
	<div class="about">
		<div class="row">
			<div class="col-md-4">
				<div class="vision">
					
					 <div class="icon"><i class="ion-ios-eye"></i></div>
				</div>

					<div class="about-title">
						VISION
					</div>

				<div class="about-text">
						<?php
							while($row_about = mysqli_fetch_array($result_about)) {
						?>
						<?php  
							if($row_about["title"]=="vision"){
						?>
							<p  id="vision">
									
									<?php echo $row_about["content"]; ?>
							</p>
					
				</div>
				
			</div>
			<div class="col-md-4" >
				<div class="mission">
					<div class="icon"><i class="ion-disc"></i></div>
				</div>

					<div class="about-title">
						MISSION
					</div>

				<div class="about-text">
						<?php 
						} 
						if($row_about["title"]=="mission"){ 	?>
							<p  id="mission">
								<?php echo $row_about["content"]; ?>
							</p>
							<?php
								
						 	
							?>
				</div>
			</div>
			<div class="col-md-4">
				<div class="goals">
					<div class="icon"><i class="ion-ios-checkmark-outline"></i></div>
				</div>
					<div class="about-title">
						GOALS
					</div>
				<div class="about-text">
						<?php 
						} 
						if($row_about["title"]=="goals"){ 	?>
							<p  id="goals">
								<?php echo $row_about["content"]; ?>
							</p>
							<?php
								} 
						 }
							?>
				</div>

					
				</div>
				
			</div>

		</div>
	</div>
</div>
</div>