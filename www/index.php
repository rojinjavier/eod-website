<!DOCTYPE html>
<html lang="en">

	<?php require_once('head.php'); ?>

<body style="background-color: #cbcbcb">

	<?php
	switch (base64_decode($_GET['r'])) {
    case 1 : 
      require_once("../www/home.php");
      break;
    case 2 : 
      require_once("../www/courses-page.php");
      break;
    case 3 : 
      require_once("../www/events-page.php");
      break;
    case 4 : 
      require_once("../www/about-page.php");
      break;

	}
	?>
  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
	<script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="../lib/easing/easing.min.js"></script>
  <script src="../lib/superfish/hoverIntent.js"></script>
  <script src="../lib/superfish/superfish.min.js"></script>
  <script src="../lib/wow/wow.min.js"></script>
  <script src="../lib/waypoints/waypoints.min.js"></script>
  <script src="../lib/counterup/counterup.min.js"></script>
  <script src="../lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="../lib/isotope/isotope.pkgd.min.js"></script>
  <script src="../lib/lightbox/js/lightbox.min.js"></script>
  <script src="../lib/touchSwipe/jquery.touchSwipe.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="../contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="../js/main.js"></script>
  <!-- <script src="../js/MyScroll.js"></script> -->
  <!-- <script src="../js/forEmail.js"></script> -->
</body>
</html>

