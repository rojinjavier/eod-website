
	<?php
    require_once "../php/connection.php";
    $sql = "SELECT * from officials ORDER BY id ASC"; 
    $result = mysqli_query($conn, $sql);


    $sql2 = "SELECT * from home_text"; 
    $result2 = mysqli_query($conn, $sql2);

	?>	


<div class="about-officials">
	<div class="title">
		EOD OFFICIALS
	</div>
	<div class="sub">
		<?php
							while($row_title2 = mysqli_fetch_array($result2)) {
						?>
						<?php  
							if($row_title2["title"]=="about_officials"){
						?>
							<p  id="about_officials" contenteditable="true">
									
									<?php echo $row_title2["content"]; ?>
							</p><?php  }}?>
	</div>
	<div class="content">
		<div class="container">

			<div class="row">
				<?php
		while($row = mysqli_fetch_array($result)) {
		?>


				<div class="col-md-4">
						<div class="card" style="width: 18rem;">
							
						  <div class="card-body">
						  	 <img src="../php/officials_view.php?image_id=<?php echo $row["id"]; ?>" class="imgs"/>
						    <p class="official-name" style=" text-transform: uppercase;"><?php echo $row['official_name'];?> <br><span class="official-position"  style=" text-transform: capitalize;"><?php echo $row['title'];?></span></p>
						    <p class="official-description"><?php echo $row['short_description'];?> </p>

		
						  </div>

						</div>
				
				</div>


			<?php		
			}
		 
		?>
			</div>
		</div>
	</div>
</div>