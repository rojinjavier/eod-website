
	<?php
	require '../php/connection.php';
    $sqlb = "SELECT id,course_name,course_description from courses ORDER BY id DESC"; 
    $resultb = mysqli_query($conn, $sqlb);
  $sql_title_courses = "SELECT * from home_text"; 
    $result_title_courses = mysqli_query($conn, $sql_title_courses);
	?>

<div style="background-color: #cbcbcb">
<div class="container"  >
	<div class="courses">
		<div class="courses-title">
			AVAILABLE COURSES
		</div>
		<div class="courses-sub">
		<?php
							while($row_title_courses = mysqli_fetch_array($result_title_courses)) {
						?>
						<?php  
							if($row_title_courses["title"]=="courses_sub"){
						?>
							<p  id="courses_sub" >
									
									<?php echo $row_title_courses["content"]; ?>
							</p><?php  }}?>
				
		</div>

		<div class="courses-content">
			<div class="row" > 

				<?php
						while($rowb = mysqli_fetch_array($resultb)) {
					?>
				<div class="col-md-4">
						<div class="card">
						 <img src="../php/course-view.php?image_id=<?php echo $rowb["id"]; ?>"  />
						  <div class="card-footer">
						  		<div class="card-footer-title">
						  			<?php echo $rowb['course_name'];?>
						  		
						  		</div>
						  		<div class="card-footer-text">
						  				<?php echo $rowb['course_description'];?>
						  		</div>
						  		<div class="form-row text-center">
								    <div class="col-12">
								    	<button type="button" id="one-course" onclick="readmore('<?php echo $rowb["id"]; ?>' , '<?php echo $rowb["course_name"]; ?>' , '<?php echo $rowb["course_description"]; ?>'  )" class="btn ">READ MORE</button>
								    </div>
								</div>
						  		
						  </div>
						</div>	<br>
				</div>
				
		
								<?php		
			}
		    
		?>
			 
			</div>

			
			
		</div>
	</div>
</div>
</div>

 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
      	<img id="imgs">
        <div class="modal-header">
  			
          <div class="modal-title" id="title" style="font-size: 24px;font-weight: bold"></div>
        </div>
        <div class="modal-body">
          <p id="desc"></p>
    
        </div>
        <div class="modal-footer">
    
           <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	function readmore(a,b,c){
	document.getElementById('imgs').src = '../php/course-view.php?image_id='+a;
	document.getElementById('title').innerHTML = b;
	document.getElementById('desc').innerHTML = c;

$('#myModal').modal('show') ;


	}


	
</script>

